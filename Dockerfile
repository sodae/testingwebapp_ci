FROM php:cli as php_srv
COPY ./app/ /var/www/app
WORKDIR /var/www/app

FROM composer:latest
WORKDIR /var/www
COPY --from=php_srv /var/www/app .
RUN composer install

CMD php -S 0.0.0.0:8000 -t www
